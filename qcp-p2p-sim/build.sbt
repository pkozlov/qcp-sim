scalaVersion := "2.10.2"

libraryDependencies ++= Seq(
  "org.scala-lang" % "scala-swing" % "2.10.2",
  "com.typesafe.akka" %% "akka-actor" % "2.3.2",
  "com.typesafe" %% "scalalogging-slf4j" % "1.1.0",
  "org.slf4j" % "slf4j-log4j12" % "1.7.7",
  "com.typesafe.slick" %% "slick" % "2.0.2",
  "com.h2database" % "h2" % "1.4.178"
)
