package qcp.p2p.db

import java.net.InetSocketAddress
import java.util.Date

import qcp.p2p.db.Direction.Direction

object Direction extends Enumeration {
  type Direction = Value
  val Incoming = Value("Incoming")
  val Outgoing = Value("Outgoing")
}

case class Message(ts: Date, address: InetSocketAddress, direction: Direction, text: String)
