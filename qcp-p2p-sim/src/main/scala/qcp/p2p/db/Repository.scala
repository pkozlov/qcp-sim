package qcp.p2p.db

import java.net.InetSocketAddress
import java.sql.Timestamp

import scala.slick.driver.H2Driver.simple._
import scala.slick.jdbc.meta.MTable

object Repository {

  private val path = System.getProperty("db.path", "./qcp-sim-db")
  val database = Database.forURL(s"jdbc:h2:$path", driver = "org.h2.Driver")

  private class Messages(tag: Tag) extends Table[(Long, Timestamp, String, Int, String, String)](tag, "messages") {
    def id = column[Long]("id", O.PrimaryKey, O.AutoInc)

    def ts = column[Timestamp]("ts")

    def hostname = column[String]("hostname")

    def port = column[Int]("port")

    def direction = column[String]("direction")

    def text = column[String]("text")

    def * = (id, ts, hostname, port, direction, text)
  }

  private val query = TableQuery[Messages]

  database withSession {
    implicit session => if (MTable.getTables("messages").list.isEmpty) query.ddl.create
  }

  def insertMessage(message: Message) = database withSession {
    implicit session =>
      val ts = new Timestamp(message.ts.getTime)
      val address = message.address
      query +=(0, ts, address.getHostName, address.getPort, message.direction.toString, message.text)
  }

  def lastMessages(num: Int) = database withSession {
    implicit session =>
      for {
        (_, ts, hostname, port, direction, text) <- query.sortBy(_.ts.desc).take(num).list.reverse
      } yield Message(ts, new InetSocketAddress(hostname, port), Direction.withName(direction), text)
  }

}
