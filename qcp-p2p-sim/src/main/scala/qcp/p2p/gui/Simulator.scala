package qcp.p2p.gui

import java.net.InetSocketAddress
import java.text.SimpleDateFormat
import java.util.Calendar

import akka.actor._
import akka.io.Tcp.Unbind
import com.typesafe.scalalogging.slf4j._
import qcp.p2p.db._
import qcp.p2p.impl._

import scala.concurrent.ExecutionContext.Implicits.global
import scala.concurrent._
import scala.swing.BorderPanel.Position._
import scala.swing._
import scala.swing.event.ButtonClicked

object Simulator extends SimpleSwingApplication with Logging {

  val timeFormat = new SimpleDateFormat("HH:mm:ss.SSS")

  // GUI elements
  val textArea = new TextArea(15, 10) {
    lineWrap = true
    wordWrap = true
    editable = false
  }

  val system = ActorSystem()
  val server = system.actorOf(Props(new Server(textArea)), "server")

  val bindPortLabel = new Label("Port")
  val bindPortField = new TextField(5)
  val bindButton = new Button("Bind")

  val hostnameLabel = new Label("Hostname")
  val hostnameField = new TextField(15) {
    text = "localhost"
  }
  val portLabel = new Label("Port")
  val portField = new TextField(5)
  val messageLabel = new Label("Message")
  val messageField = new TextField(20)
  val sendButton = new Button("Send")

  case class Config(port: Int = 2001)

  override def shutdown() {
    system.shutdown()
  }

  override def top = new MainFrame {
    title = "QCP P2P Simulator"

    contents = new BorderPanel {
      layout(new BoxPanel(Orientation.Horizontal) {
        contents += new FlowPanel(bindPortLabel, bindPortField)
        contents += bindButton
      }) = North
      layout(new ScrollPane(textArea)) = Center
      layout(new BoxPanel(Orientation.Horizontal) {
        contents += new FlowPanel(hostnameLabel, hostnameField)
        contents += new FlowPanel(portLabel, portField)
        contents += new FlowPanel(messageLabel, messageField)
        contents += sendButton
      }) = South
    }

    future {
      for {
        message <- Repository.lastMessages(10)
        time = timeFormat.format(message.ts)
        delimiter = message.direction match {
          case Direction.Incoming => "=>"
          case Direction.Outgoing => "<="
        }
        string = s"$time: ${message.address} $delimiter ${message.text}\n"
      } Swing.onEDT(textArea append string)
    }

    listenTo(sendButton, bindButton, textArea)

    reactions += {

      case ButtonClicked(`sendButton`) =>
        val address = new InetSocketAddress(hostnameField.text, portField.text.toInt)
        val message = messageField.text
        system.actorOf(Props(new Client(message, address, textArea)))
        val time = timeFormat.format(Calendar.getInstance().getTime)
        textArea append s"$time: $address <= $message\n"

      case ButtonClicked(`bindButton`) =>
        server ! (bindButton.text match {
          case "Bind" => bindPortField.text.toInt
          case "Unbind" => Unbind
        })
        bindButton.enabled = false

      case BoundEvent(_) =>
        bindButton.enabled = true
        bindButton.text = "Unbind"

      case UnboundEvent(_) =>
        bindButton.enabled = true
        bindButton.text = "Bind"

      case BindFailedEvent(_) =>
        bindButton.enabled = true
        Dialog.showMessage(bindButton, "Fail to bind", "Error", Dialog.Message.Error)

      case MessageEvent(ts, address, message) =>
        val time = timeFormat.format(ts)
        textArea append s"$time: $address => $message\n"

      case KeyError =>
        textArea append s"Key exchange error!\n"

    }

  }

}
