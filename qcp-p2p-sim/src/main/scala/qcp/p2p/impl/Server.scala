package qcp.p2p.impl

import java.net.InetSocketAddress

import akka.actor._
import akka.io.Tcp._
import akka.io._

import scala.swing.Publisher

class Server(publisher: Publisher) extends Actor with ActorLogging {

  import context.system

  override def receive = {

    case port: Int => IO(Tcp) ! Bind(self, new InetSocketAddress("0.0.0.0", port))

    case b@Bound(localAddress) =>
      log.info(s"Bound address $localAddress")
      publisher.publish(BoundEvent(b))
      context.become(running(sender()), discardOld = false)

    case CommandFailed(b: Bind) =>
      log.error(s"Bind $b failed")
      publisher.publish(BindFailedEvent(b))
  }

  def running(socket: ActorRef): Receive = {

    case c@Connected(remote, local) =>
      log.info(s"Connected remote=$remote, local=$local")
      val connection = sender()
      val handler = context.actorOf(Props(new ServerHandler(connection, remote, publisher)))
      sender ! Register(handler)

    case Unbind => socket ! Unbind

    case Unbound =>
      log.info("Unbound")
      publisher.publish(UnboundEvent(Unbound))
      context.unbecome()

  }

}
