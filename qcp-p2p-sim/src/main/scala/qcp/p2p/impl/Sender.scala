package qcp.p2p.impl

import java.net.InetSocketAddress
import java.util.Calendar

import akka.actor._
import akka.io.Tcp._
import akka.serialization._
import akka.util._
import qcp.p2p.db._

import scala.swing.Publisher
import scala.util._

class Sender(text: String, publisher: Publisher, remote: InetSocketAddress) extends Actor with ActorLogging {

  val rand = new Random
  implicit val serializer = SerializationExtension(context.system)
  val size = text.size * 40

  log.info("Generating bit sequence")
  val bitSeq = rand.booleans(size)
  log.debug("Bit sequence {}", bitSeq)

  log.info("Generating operations sequence")
  val opSeq = rand.booleans(size)
  log.debug("Operations sequence {}", opSeq)

  val lists = for {
    (op, bit) <- opSeq zip bitSeq
  } yield if (op) (rand.nextBoolean(), bit) else (bit, rand.nextBoolean())

  context.parent ! Write(ByteString.fromArray(serializer.serialize(lists.toArray).get))

  override def receive = {

    case data: ByteString =>
      val booleans = deserialize(data, classOf[Array[Boolean]])
      val indices = for {
        (i, op1, op2) <- (0 until size, opSeq, booleans).zipped.toArray
        if op1 == op2
      } yield i
      log.info(s"Sending indices, size ${indices.size}")
      val matched = subListByIndices(bitSeq.toList, indices)
      context.parent ! Write(ByteString.fromArray(serializer.serialize(indices).get))
      context.become(step1(matched))
  }

  def step1(matched: List[Boolean]): Receive = {

    case data: ByteString =>
      val control = deserialize(data, classOf[Array[Boolean]])
      log.info(s"Received control bits ${control.size}")
      val (part1, part2) = matched.splitAt(matched.size / 2)
      val key = part2.grouped(8).toList.map(_.toInt)
      log.info(s"key=$key")
      if (part1 == control.toList) {
        log.info("Encrypting and sending message")
        val cypher = for ((b, k) <- text zip key) yield (b ^ k).toChar
        context.parent ! Write(ByteString.fromArray(serializer.serialize(cypher.toArray).get))
        Repository.insertMessage(Message(Calendar.getInstance.getTime, remote, Direction.Outgoing, text))
      } else {
        publisher publish KeyError
        log.error("Key exchange error!")
      }
      context.parent ! ConfirmedClose
  }

}
