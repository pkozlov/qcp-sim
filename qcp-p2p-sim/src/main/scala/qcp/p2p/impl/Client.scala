package qcp.p2p.impl

import java.net._

import akka.actor._
import akka.io.Tcp._
import akka.io._

import scala.swing.Publisher

class Client(message: String, address: InetSocketAddress, publisher: Publisher) extends Actor with ActorLogging {

  import context.system

  log.info("Sending message '{}' to address {}", message, address)
  IO(Tcp) ! Connect(address)

  override def receive = {

    case Connected(remote, local) =>
      log.info("Connected, remote={}, local={}", remote, local)
      val connection = sender()
      val handler = context.actorOf(Props(new ClientHandler(message, connection, publisher, remote)))
      connection ! Register(handler)

    case c: ConnectionClosed =>
      log.info(s"Connection closed $c")
      context stop self

  }

}
