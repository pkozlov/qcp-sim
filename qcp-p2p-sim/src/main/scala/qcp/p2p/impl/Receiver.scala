package qcp.p2p.impl

import java.net.InetSocketAddress
import java.util.Calendar

import akka.actor._
import akka.io.Tcp._
import akka.serialization._
import akka.util._
import qcp.p2p.db._

import scala.swing._
import scala.util._

class Receiver(address: InetSocketAddress, publisher: Publisher) extends Actor with ActorLogging {

  val rand = new Random
  implicit val serializer = SerializationExtension(context.system)

  override def receive = step0

  val step0: Receive = {
    case data: ByteString =>
      val lists = deserialize(data, classOf[Array[(Boolean, Boolean)]])
      log.info(s"Received list, size ${lists.size}")
      val opSeq = rand.booleans(lists.size)
      val bits = for {
        (op, b) <- opSeq zip lists
      } yield if (op) b._2 else b._1
      log.info(s"Read bits, size ${bits.size}")
      context.parent ! Write(serialize(opSeq.toArray))
      context.become(step1(bits))
  }

  def step1(bits: IndexedSeq[Boolean]): Receive = {
    case data: ByteString =>
      val indices = deserialize(data, classOf[Array[Int]])
      log.info(s"Received indices ${indices.size}")
      val matched = subListByIndices(bits.toList, indices)
      val (part1, part2) = matched.splitAt(matched.size / 2)
      log.info("Sending control bits")
      context.parent ! Write(serialize(part1.toArray))
      val key = part2.grouped(8).toList.map(_.toInt)
      log.info(s"key=$key")
      context.become(step2(key))
  }

  def step2(key: List[Int]): Receive = {
    case data: ByteString =>
      val cypher = deserialize(data, classOf[Array[Char]])
      log.info("Received encrypted text, decrypting")
      val text = (for ((b, k) <- cypher zip key) yield (b ^ k).toChar).mkString
      val ts = Calendar.getInstance.getTime
      publisher.publish(MessageEvent(ts, address, text))
      log.info(s"Decrypted text '$text'")
      Repository.insertMessage(Message(ts, address, Direction.Incoming, text))

  }

}
