package qcp.p2p

import java.net._
import java.util.Date

import akka.io.Tcp._
import akka.serialization._
import akka.util._

import scala.swing.event.Event
import scala.util._

package object impl {

  case class BoundEvent(bound: Bound) extends Event

  case class UnboundEvent(unbound: Unbound) extends Event

  case class BindFailedEvent(b: Bind) extends Event

  case class MessageEvent(ts: Date, address: InetSocketAddress, message: String) extends Event

  case object KeyError extends Event

  implicit class RandomOps(rand: Random) {
    def booleans(size: Int): IndexedSeq[Boolean] = for (i <- 1 to size) yield rand.nextBoolean()
  }

  implicit class ListBoolean(list: List[Boolean]) {
    def toInt = {
      val chars = list.map(if (_) '1' else '0')
      Integer.parseInt(chars.mkString, 2)
    }
  }

  def serialize(o: AnyRef)(implicit serializer: Serialization): ByteString =
    ByteString.fromArray(serializer.serialize(o).get)

  def deserialize[T](bytes: ByteString, clazz: Class[T])(implicit serializer: Serialization): T =
    serializer.deserialize(bytes.toArray, clazz).get

  def randomBoolSeq(rand: Random, size: Int): Seq[Boolean] = {
    for (i <- 1 to size) yield rand.nextBoolean()
  }

  def subListByIndices[T](seq: List[T], indices: Seq[Int]): List[T] = {
    indices.foldLeft(List[T]())((acc, i) => seq(i) :: acc)
  }

}
