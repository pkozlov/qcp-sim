package qcp.p2p.impl

import java.net.InetSocketAddress
import java.nio._

import akka.actor._
import akka.io.Tcp._
import akka.util._

import scala.swing.Publisher

class ClientHandler(message: String, connection: ActorRef, publisher: Publisher,
                    remote: InetSocketAddress) extends Actor with ActorLogging {

  val messageSender = context.actorOf(Props(new Sender(message, publisher, remote)))

  override def receive = {
    case Received(data) =>
      val (part1, part2) = data.splitAt(4)
      val size = part1.toByteBuffer.getInt(0)
      log.info(s"Received size $size = $part1")
      if (part2.size >= size) messageSender ! part2
      else context.become(buffering(part2, size))

    case Write(data, ack) =>
      val size = data.size
      val bs = ByteString(ByteBuffer.allocate(4).putInt(size).array())
      log.info(s"Writing size $size = $bs")
      connection ! Write(bs ++ data, ack)

    case c: CloseCommand =>
      log.info(s"Closing connection $c")
      connection ! c

    case c: ConnectionClosed =>
      context.parent ! c
  }

  def buffering(buffer: ByteString, size: Int): Receive = {
    case Received(data) =>
      val bs = buffer ++ data
      if (bs.size >= size) {
        messageSender ! bs
        context.unbecome()
      } else context.become(buffering(bs, size))
  }

}
