package qcp.p2p.impl

import java.net._
import java.nio._

import akka.actor._
import akka.io.Tcp._
import akka.util._

import scala.swing._

class ServerHandler(connection: ActorRef, address: InetSocketAddress, publisher: Publisher) extends Actor with ActorLogging {

  val receiver = context.actorOf(Props(new Receiver(address, publisher)))

  override def receive = {
    case Received(data) =>
      val (part1, part2) = data.splitAt(4)
      val size = part1.toByteBuffer.getInt(0)
      log.info(s"Received size $size = $part1")
      if (part2.size >= size) receiver ! part2
      else context.become(buffering(part2, size))

    case Write(data, ack) =>
      val size = data.size
      val bs = ByteString(ByteBuffer.allocate(4).putInt(size).array())
      log.info(s"Writing size $size = $bs")
      connection ! Write(bs ++ data, ack)

    case c: ConnectionClosed =>
      log.info(s"Connection closed $c")
      context stop self
  }

  def buffering(buffer: ByteString, size: Int): Receive = {
    case Received(data) =>
      val bs = buffer ++ data
      if (bs.size >= size) {
        receiver ! bs
        context.unbecome()
      } else context.become(buffering(bs, size))
  }

}
