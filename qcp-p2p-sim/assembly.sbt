import AssemblyKeys._
assemblySettings

mainClass in assembly := Some("qcp.p2p.gui.Simulator")

jarName in assembly := "qcp-p2p-sim.jar"
