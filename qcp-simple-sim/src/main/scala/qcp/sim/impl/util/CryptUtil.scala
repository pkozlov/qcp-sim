package qcp.sim.impl.util

import scala.util.Random

object CryptUtil {

  def randomBoolSeq(rand: Random, size: Int): Seq[Boolean] = {
    for (i <- 1 to size) yield rand.nextBoolean()
  }

  def randomBinarySeq(rand: Random, size: Int): Seq[Char] = {
    randomBoolSeq(rand, size) map (if (_) '1' else '0')
  }

  def subListByIndices[T](seq: List[T], indices: Seq[Int]): List[T] = {
    indices.foldLeft(List[T]())((acc, i) => seq(i) :: acc)
  }

}
