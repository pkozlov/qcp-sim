package qcp.sim.impl.people

import scala.util.Random
import qcp.sim.impl.util.CryptUtil
import qcp.sim.impl.channel._
import com.typesafe.scalalogging.slf4j.Logging

class Person(val name: String, qc: QuantumChannel, sc: SimpleChannel) extends Logging {

  val rand = new Random()

  var bitSeq: Seq[Char] = Seq.empty
  var operationSeq: Seq[Boolean] = Seq.empty
  var indices: Seq[Int] = Seq.empty
  var matched: Seq[Char] = Seq.empty
  var control: Seq[Char] = Seq.empty
  var key: List[Int] = List.empty
  var ok: Boolean = false

  def genOperationSeq(size: Int) {
    logger.info("Generating operations sequence")
    operationSeq = CryptUtil.randomBoolSeq(rand, size)
    logger.debug("Operations sequence {}", operationSeq)
  }

  def genBitSeq(size: Int) {
    logger.info("Generating bit sequence")
    bitSeq = CryptUtil.randomBinarySeq(rand, size)
    logger.debug("Bit sequence {}", new String(bitSeq.toArray))
  }

  def writeToQuantumChannel() {
    logger.info("Writing qubits to quantum channel")
    qc.write(bitSeq, operationSeq)
  }

  def readFromQuantumChannel() {
    logger.info("Reading qubits from quantum channel")
    bitSeq = qc.read(operationSeq)
  }

  def writeOperations() {
    logger.info("Writing operations sequence to simple channel")
    sc.writeOperations(operationSeq)
  }

  def readOperations() {
    logger.info("Reading operations sequence from simple channel")
    val other = sc.readOperations()
    indices = for {
      (i, op1, op2) <- (0 until operationSeq.size, operationSeq, other).zipped.toList
      if op1 == op2
    } yield i
    matched = CryptUtil.subListByIndices(bitSeq.toList, indices)
    logger.info(s"Found ${matched.size} matched bits")
    logger.debug("Matched bits {}", new String(matched.toArray))
  }

  def writeIndices() {
    logger.info("Writing indices list to simple channel")
    sc.writeIndices(indices)
  }

  def readIndices() {
    logger.info("Reading indices list from simple channel")
    indices = sc.readIndices()
    matched = CryptUtil.subListByIndices(bitSeq.toList, indices)
    logger.info(s"Found ${matched.size} matched bits")
    logger.debug("Matched bits {}", new String(matched.toArray))
  }

  def writeControl() {
    logger.info("Writing control bits to simple channel")
    val (part1, part2) = matched.splitAt(matched.size / 2)
    key = part2.grouped(8).toList.map(chars => Integer.parseInt(new String(chars.toArray)))
    control = part1
    logger.debug("Possible key {}", key)
    logger.debug("Control bits {}", new String(control.toArray))
    sc.write(control)
  }

  def readControl() {
    logger.info("Reading control bits from simple channel")
    val (part1, part2) = matched.splitAt(matched.size / 2)
    key = part2.grouped(8).toList.map(chars => Integer.parseInt(new String(chars.toArray)))
    control = part1
    val other = sc.read()
    ok = other == control
    if (ok) {
      logger.info("Key exchange finished")
      logger.debug("Obtained key {}", key)
    }
    else logger.error("Key exchange failed, controls don't match, expected = {}, actual = {}", control, other)
    ok
  }

  def sendData(data: String) {
    assert(ok, "Key exchange is not established")
    logger.info("Sending encrypted data '{}'", data)
    sc.write(for ((b, k) <- data zip key) yield (b ^ k).toChar)
  }

  def receiveData() {
    val data = for ((b, k) <- sc.read() zip key) yield (b ^ k).toChar
    logger.info("Receiving and decrypting data '{}'", new String(data.toArray))
  }

}
