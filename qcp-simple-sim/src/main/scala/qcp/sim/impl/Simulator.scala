package qcp.sim.impl

import qcp.sim.impl.people.Person
import qcp.sim.impl.channel._
import com.typesafe.scalalogging.slf4j.Logging

class Simulator extends Logging {

  var people: Map[String, Person] = Map.empty

  val sc = new SimpleChannel
  val qc = new QuantumChannel

  def addPerson(name: String) {
    assert(name != null && !name.isEmpty, "Name can't be empty")
    assert(!people.contains(name), s"$name already exists")
    people += name -> new Person(name, qc, sc)
    logger.info("Added '{}' person", name)
  }

  def removePerson(name: String) {
    assert(name != null && !name.isEmpty, "Name can't be empty")
    assert(people.contains(name), s"$name doesn't exist")
    people -= name
    logger.info("Removed '{}' person", name)
  }

  def names() = people.keys.toList

  def send(from: String, to: String, message: String) {
    val sender = people(from)
    val recipient = people(to)
    logger.info("Sending message '{}' from {} to {}", message, from, to)

    val size = message.size * 40
    logger.info(s"Bit sequence size $size")

    sender.genBitSeq(size)
    sender.genOperationSeq(size)
    sender.writeToQuantumChannel()
    recipient.genOperationSeq(size)
    recipient.readFromQuantumChannel()
    recipient.writeOperations()
    sender.readOperations()
    sender.writeIndices()
    recipient.readIndices()

    recipient.writeControl()
    sender.readControl()

    sender.sendData(message)
    recipient.receiveData()
  }

}

