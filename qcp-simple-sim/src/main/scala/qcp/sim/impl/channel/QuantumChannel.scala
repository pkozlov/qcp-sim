package qcp.sim.impl.channel

import qcp.sim.impl.qubit.Qubit

class QuantumChannel {

  var qubitSeq: Seq[Qubit] = Seq.empty

  def write(bitSeq: Seq[Char], operationSeq: Seq[Boolean]) {
    qubitSeq = Qubit.fromBinarySeq(bitSeq)
    for {
      (qubit, op) <- qubitSeq zip operationSeq
      if op
    } qubit.transform(Qubit.hadamard)
  }

  def read(operationSeq: Seq[Boolean]) = {
    for {
      (qubit, operation) <- qubitSeq zip operationSeq
      if operation
    } qubit.transform(Qubit.hadamard)
    qubitSeq.toBinarySeq
  }
}
