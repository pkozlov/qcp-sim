package qcp.sim.impl.channel

class SimpleChannel {

  var operationSeq: Seq[Boolean] = Seq.empty
  var indices: Seq[Int] = Seq.empty
  var control: Seq[Char] = Seq.empty
  var data: Seq[Char] = Seq.empty

  def writeOperations(operationSeq: Seq[Boolean]) {
    this.operationSeq = operationSeq
  }

  def readOperations() = operationSeq

  def writeIndices(indices: Seq[Int]) {
    this.indices = indices
  }

  def readIndices() = indices

  def write(data: Seq[Char]) {
    this.data = data
  }

  def read() = data
}
