package qcp.sim.impl.qubit

import scala.util.Random
import qcp.sim.impl.qubit.Qubit._

case class Qubit(var state: QubitState) {

  val random = new Random
  random.setSeed(System.currentTimeMillis())

  def value: Char = state match {
    case Q0 => '0'
    case Q1 => '1'
    case _ => random.shuffle(Seq('0', '1')).head
  }

  def transform(operation: QubitState => QubitState) {
    state = operation(state)
  }

}

object Qubit {

  sealed trait QubitState

  case object Q0 extends QubitState

  case object Q1 extends QubitState

  case object QP extends QubitState

  case object QM extends QubitState

  implicit class QubitSeq(val qubits: Seq[Qubit]) {
    def toBinarySeq: Seq[Char] = qubits.map(_.value)
  }

  def fromBit: Char => Qubit = {
    case '0' => Qubit(Q0)
    case '1' => Qubit(Q1)
  }

  def fromBinarySeq: Seq[Char] => Seq[Qubit] = x => x map fromBit

  def hadamard: QubitState => QubitState = {
    case Q0 => QP
    case Q1 => QM
    case QP => Q0
    case QM => Q1
  }

}
