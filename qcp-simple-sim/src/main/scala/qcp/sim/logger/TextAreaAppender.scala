package qcp.sim.logger

import org.apache.log4j.AppenderSkeleton
import scala.swing.TextArea
import org.apache.log4j.spi.LoggingEvent

class TextAreaAppender extends AppenderSkeleton {

  var textArea: TextArea = null

  override def append(event: LoggingEvent) {
    if (textArea != null) textArea.append(layout.format(event))
  }

  override val requiresLayout = true

  override def close(): Unit = ()

}
