package qcp.sim.gui

import swing._
import event._
import Swing._
import javax.swing._

import com.typesafe.scalalogging.slf4j.Logging
import org.apache.log4j.LogManager

import scala.concurrent._
import ExecutionContext.Implicits.global

import qcp.sim.impl.Simulator
import qcp.sim.logger.TextAreaAppender

object QcpSimulatorApp extends SimpleSwingApplication with Logging {

  val sim = new Simulator

  override def top = new MainFrame {

    import BorderPanel.Position._

    title = "QCP Simulator"
    preferredSize = new Dimension(640, 480)
    val loggerArea = new TextArea {
      lineWrap = true
      wordWrap = true
      editable = false
    }

    LogManager.getRootLogger.getAppender("gui") match {
      case appender: TextAreaAppender =>
        appender.textArea = loggerArea
      case _ =>
        logger.warn("Cannot get appender names 'gui'")
    }

    val addLabel = new Label("Add person")
    val addField = new TextField(10)
    val addButton = new Button("Add")

    val removeLabel = new Label("Remove person")
    val removeModel = new DefaultComboBoxModel[String]
    val removeComboBox = new ComboBox[String](sim.names()) {
      peer.asInstanceOf[JComboBox[String]].setModel(removeModel)
    }
    val removeButton = new Button("Remove")

    val sendLabel = new Label("Send message")
    val sendFromModel = new DefaultComboBoxModel[String]
    val sendFromComboBox = new ComboBox(sim.names()) {
      peer.asInstanceOf[JComboBox[String]].setModel(sendFromModel)
    }
    val sendToModel = new DefaultComboBoxModel[String]
    val sendToComboBox = new ComboBox(sim.names()) {
      peer.asInstanceOf[JComboBox[String]].setModel(sendToModel)
    }
    val sendField = new TextField(15)
    val sendButton = new Button("Send")

    contents = new BorderPanel {
      layout(new ScrollPane(loggerArea)) = Center
      layout(new BoxPanel(Orientation.Vertical) {
        contents += new FlowPanel(addLabel, addField, addButton)
        contents += new FlowPanel(removeLabel, removeComboBox, removeButton)
        contents += new FlowPanel(sendLabel, sendFromComboBox, sendToComboBox, sendField, sendButton)
      }) = South
    }

    listenTo(addButton, removeButton, sendButton)

    reactions += {
      case ButtonClicked(button) =>
        button match {
          case `addButton` =>
            val name = addField.text
            val f = future {
              sim.addPerson(name)
            }

            f onSuccess {
              case _ => onEDT {
                removeModel.addElement(name)
                sendFromModel.addElement(name)
                sendToModel.addElement(name)
              }
            }

            f onFailure {
              case t => showErrorDialog(button, t)
            }

          case `removeButton` =>
            val item = removeComboBox.selection.item
            val f = future {
              sim.removePerson(item)
            }

            f onSuccess {
              case _ => onEDT {
                removeModel.removeElement(item)
                sendFromModel.removeElement(item)
                sendToModel.removeElement(item)
              }
            }

            f onFailure {
              case t => showErrorDialog(button, t)
            }

          case `sendButton` =>
            val from = sendFromComboBox.selection.item
            val to = sendToComboBox.selection.item
            val message = sendField.text
            val f = future(sim.send(from, to, message))
            f onFailure {
              case t => showErrorDialog(button, t)
            }

        }
    }

  }

  private def showErrorDialog(c: Component, t: Throwable) = onEDT {
    val cause = t.getCause
    Dialog.showMessage(c, s"$cause:  ${cause.getMessage}", "Error!", Dialog.Message.Error)
  }
}
