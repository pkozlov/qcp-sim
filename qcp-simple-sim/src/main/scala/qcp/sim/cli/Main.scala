package qcp.sim.cli

import qcp.sim.impl.Simulator

object Main extends App {
  val sim = new Simulator
  sim.addPerson("Alice")
  sim.addPerson("Bob")
  sim.send("Alice", "Bob", "Hello!")
}
