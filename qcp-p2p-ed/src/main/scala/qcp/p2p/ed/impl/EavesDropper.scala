package qcp.p2p.ed.impl

import akka.actor._
import akka.io.Tcp._
import akka.util._
import akka.serialization._

import scala.util._

import EavesDropper._

class EavesDropper(client: ActorRef, server: ActorRef) extends Actor with ActorLogging {

  val rand = new Random()

  implicit val serializer = SerializationExtension(context.system)

  override def receive = step0

  val doNothing: Receive = {
    case Drop(connection, data) => connection ! Write(writeInt(data.size) ++ data)
  }

  val step0: Receive = {

    case Drop(`server`, data) =>
      val oldData = deserialize(data, classOf[Array[(Boolean, Boolean)]])
      log.info(s"Intercepted list, size ${oldData.size}")
      val opSeq = rand.booleans(oldData.size)
      val bits = for {
        (op, b) <- opSeq zip oldData
      } yield if (op) b._2 else b._1
      log.info(s"Read bits, size ${bits.size}")

      val newData = for (bit <- bits) yield (bit, bit)
      val bs = serialize(newData.toArray)
      server ! Write(writeInt(bs.size) ++ bs)
      context.become(doNothing)

  }

}

object EavesDropper {

  case class Drop(connection: ActorRef, data: ByteString)

}
