package qcp.p2p.ed.cli

import akka.actor._

import qcp.p2p.ed.impl._
import scopt.OptionParser

object Main extends App {

  case class Config(remoteHost: String = "", remotePort: Int = 0, bindPort: Int = 0)

  val parser = new OptionParser[Config]("qcp-p2p-ed") {
    head("")
    arg[String]("remoteHost") action {
      (h, c) => c.copy(remoteHost = h)
    }
    arg[Int]("remotePort") action {
      (p, c) => c.copy(remotePort = p)
    }
    arg[Int]("bindPort") action {
      (p, c) => c.copy(bindPort = p)
    }
  }

  parser.parse(args, Config()) map {
    config =>
      val system = ActorSystem("qcp-p2p-ed")
      system.actorOf(Props(new FakeServer(config)))
      sys addShutdownHook {
        system.shutdown()
      }
  }

}
