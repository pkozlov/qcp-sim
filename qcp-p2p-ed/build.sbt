scalaVersion := "2.10.2"

libraryDependencies ++= Seq(
  "com.github.scopt" %% "scopt" % "3.2.0",
  "com.typesafe.akka" %% "akka-actor" % "2.3.2",
  "com.typesafe" %% "scalalogging-slf4j" % "1.1.0",
  "org.slf4j" % "slf4j-log4j12" % "1.7.7"
)

