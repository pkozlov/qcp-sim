Quantum Cryptography Protocols Simulator
========================================
* qcp-simple-sim - Simple Swing-based networkless GUI application which represents BB84 quantum key distribution protocol.
* qcp-p2p-sim - Point-to-Point network simulator. Each instance has its own TCP server and TCP client. It can send messages to other TCP servers which serialized and encrypted with OTP (One-Time-Pad) protocol. Key exchange is established using simulation of BB84 protocol using two-channels superposition.
* qcp-p2p-ed - eavesdropper for p2p version of simulator. Listens to one TCP port and intercepts messages between server and client.

