package qcp.p2p.eda.impl

import akka.actor._
import akka.io._
import akka.io.Tcp._
import java.net.InetSocketAddress
import qcp.p2p.eda.cli.Main.Config
import akka.io.Tcp

class FakeServer(config: Config) extends Actor with ActorLogging {

  import context.system

  IO(Tcp) ! Bind(self, new InetSocketAddress("0.0.0.0", config.bindPort))

  override def receive = {
    case b: Bound =>
      log.info(s"Bound $b")
      context.become {
        case Connected(remote, local) =>
          val address = new InetSocketAddress(config.remoteHost, config.remotePort)
          val connection = sender()
          context.actorOf(Props(new FakeClient(address, connection)))
      }
  }

}
