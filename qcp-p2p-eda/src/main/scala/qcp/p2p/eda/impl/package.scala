package qcp.p2p.eda

import akka.serialization.Serialization
import akka.util.ByteString
import scala.util.Random
import java.nio.ByteBuffer

package object impl {

  def writeInt(i: Int): ByteString = ByteString(ByteBuffer.allocate(4).putInt(i).array())

  def readInt(bs: ByteString) = bs.toByteBuffer.getInt(0)

  def serialize(o: AnyRef)(implicit serializer: Serialization): ByteString =
    ByteString.fromArray(serializer.serialize(o).get)

  def deserialize[T](bytes: ByteString, clazz: Class[T])(implicit serializer: Serialization): T =
    serializer.deserialize(bytes.toArray, clazz).get

  implicit class RandomOps(rand: Random) {
    def booleans(size: Int): IndexedSeq[Boolean] = for (i <- 1 to size) yield rand.nextBoolean()
  }

  implicit class ListBoolean(list: List[Boolean]) {
    def toInt = {
      val chars = list.map(if (_) '1' else '0')
      Integer.parseInt(chars.mkString, 2)
    }
  }

}
