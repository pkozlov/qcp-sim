package qcp.p2p.eda.impl

import akka.actor._
import akka.io.Tcp._
import akka.util.ByteString

class Handler extends Actor with ActorLogging {

  override def receive = {
    case Received(data) =>
      val (part1, part2) = data.splitAt(4)
      val size = readInt(part1)
      log.info(s"Received size $size = $part1")
      if (part2.size >= size) forward(part2)
      else context.become(buffering(part2, size))
  }

  def buffering(buffer: ByteString, size: Int): Receive = {
    case Received(data) =>
      val bs = buffer ++ data
      if (bs.size >= size) {
        forward(bs)
        context.unbecome()
      } else context.become(buffering(bs, size))
  }

  private def forward(data: ByteString) = {
    context.parent ! data
  }

}
