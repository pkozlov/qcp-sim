package qcp.p2p.eda.impl

import akka.actor._
import akka.io.Tcp._
import akka.util._
import akka.serialization._

import scala.util._

class EavesDropper(client: ActorRef, server: ActorRef) extends Actor with ActorLogging {

  import EavesDropper._

  val rand = new Random()

  implicit val serializer = SerializationExtension(context.system)

  override def receive = step0

  val doNothing: Receive = {
    case Drop(connection, data) => connection ! Write(writeInt(data.size) ++ data)
  }

  val step0: Receive = {

    case Drop(`server`, data) =>
      // 1. получить от клиента два списка
      val clientLists = deserialize(data, classOf[Array[(Boolean, Boolean)]])
      val size = clientLists.size
      log.info(s"Received list, size $size")
      val clientOpSeq = rand.booleans(size)
      val clientBits = for {
        (op, b) <- clientOpSeq zip clientLists
      } yield if (op) b._2 else b._1

      // 2. сгенерировать два списка, отправить серверу
      val serverBitSeq = rand.booleans(size)
      val serverOpSeq = rand.booleans(size)
      val serverLists = for {
        (op, bit) <- serverOpSeq zip serverBitSeq
      } yield if (op) (rand.nextBoolean(), bit) else (bit, rand.nextBoolean())
      val data1 = ByteString.fromArray(serializer.serialize(serverLists.toArray).get)
      server ! Write(writeInt(data1.size) ++ data1)

      log.info("step1")
      context.become(step1(clientOpSeq, clientBits, serverBitSeq, serverOpSeq))

  }

  def step1(clientOpSeq: Seq[Boolean], clientBits: Seq[Boolean], serverBitSeq: Seq[Boolean], serverOpSeq: Seq[Boolean]): Receive = {

    case Drop(`client`, data) =>
      // 3. получить ответ (список операций) от сервера
      val booleans = deserialize(data, classOf[Array[Boolean]])
      val size = serverOpSeq.size
      val indices = for {
        (i, op1, op2) <- (0 until size, serverOpSeq, booleans).zipped.toArray
        if op1 == op2
      } yield i

      // 4. отослать ответ (свой) клиенту
      val data1 = serialize(clientOpSeq.toArray)
      client ! Write(writeInt(data1.size) ++ data1)

      log.info("step2")
      context.become(step2(indices, serverBitSeq, clientBits))
  }

  def step2(serverIndices: Array[Int], serverBitSeq: Seq[Boolean], clientBits: Seq[Boolean]): Receive = {

    case Drop(`server`, data) =>
      // 5. получить от клиента список индексов
      val indices = deserialize(data, classOf[Array[Int]])
      log.info(s"Received indices ${indices.size}")
      val clientMatched = subListByIndices(clientBits.toList, indices)
      val (part1, part2) = clientMatched.splitAt(clientMatched.size / 2)
      val key = part2.grouped(8).toList.map(_.toInt)
      log.info(s"key=$key")

      // 6. вычислить список индексов, отправить серверу
      val serverMatched = subListByIndices(serverBitSeq.toList, serverIndices)
      val data1 = ByteString.fromArray(serializer.serialize(serverIndices).get)
      server ! Write(writeInt(data1.size) ++ data1)

      log.info("step3")
      context.become(step3(serverMatched, part1, key))

  }

  def step3(serverMatched: List[Boolean], clientPart1: Seq[Boolean], clientKey: List[Int]): Receive = {

    case Drop(`client`, data) =>
      // 7. получить от сервера проверочный код
      val control = deserialize(data, classOf[Array[Boolean]])
      // 8. отослать клиенту проверочный код (свой)
      val data1 = serialize(clientPart1.toArray)
      client ! Write(writeInt(data1.size) ++ data1)

      log.info("step4")
      context.become(step4(control, serverMatched, clientKey))

  }

  def step4(serverControl: Array[Boolean], serverMatched: List[Boolean], clientKey: List[Int]): Receive = {

    case Drop(`server`, data) =>
      // 9. получить от клиента зашифрованное сообщение, расшифровать, зашифровать своим ключом, отправить серверу
      val clientCypher = deserialize(data, classOf[Array[Char]])
      log.info("Received encrypted message, decrypting")
      val message = (for ((b, k) <- clientCypher zip clientKey) yield (b ^ k).toChar).mkString

      log.info(s"Received message '$message'")

      val (part1, part2) = serverMatched.splitAt(serverMatched.size / 2)
      val serverKey = part2.grouped(8).toList.map(_.toInt)
      if (part1 == serverControl.toList) {
        log.info("Encrypting and sending message")
        val serverCypher = for ((b, k) <- message zip serverKey) yield (b ^ k).toChar
        val data1 = ByteString.fromArray(serializer.serialize(serverCypher.toArray).get)
        server ! Write(writeInt(data1.size) ++ data1)
        context.become(step0)
      } else {
        log.error("Key exchange error!")
        context.become(doNothing)
      }

  }

}

object EavesDropper {

  def subListByIndices[T](seq: List[T], indices: Seq[Int]): List[T] = {
    indices.foldLeft(List[T]())((acc, i) => seq(i) :: acc)
  }

  case class Drop(connection: ActorRef, data: ByteString)

}
