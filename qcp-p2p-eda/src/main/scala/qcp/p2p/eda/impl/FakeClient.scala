package qcp.p2p.eda.impl

import akka.actor._
import akka.io._
import akka.io.Tcp._
import akka.util._
import java.net.InetSocketAddress

import EavesDropper._

class FakeClient(address: InetSocketAddress, client: ActorRef) extends Actor with ActorLogging {

  import context.system

  IO(Tcp) ! Connect(address)

  override def receive = {
    case Connected(remote, local) =>
      val realServer = context.actorOf(Props[Handler])
      val realClient = context.actorOf(Props[Handler])
      val server = sender()
      val eavesDropper = context.actorOf(Props(new EavesDropper(client, server)))
      client ! Register(realClient)
      server ! Register(realServer)
      context.become {
        case data: ByteString => sender() match {
          case `realServer` =>
            log.info(s"Received ${data.size} bytes from real server")
            eavesDropper ! Drop(client, data)
          case `realClient` =>
            log.info(s"Received ${data.size} bytes from real client")
            eavesDropper ! Drop(server, data)
        }
      }
  }

}
