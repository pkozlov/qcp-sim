import AssemblyKeys._
assemblySettings

mainClass in assembly := Some("qcp.p2p.eda.cli.Main")

jarName in assembly := "qcp-p2p-eda.jar"
